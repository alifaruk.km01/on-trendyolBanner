

// read text from URL location
function getJsonObject(cb){
	var request = new XMLHttpRequest();
	request.open('GET', "javascripts/myjsonfile.json", true);
	request.send(null);
	request.onreadystatechange = function () {
		if (request.readyState === 4 && request.status === 200) {
			var type = request.getResponseHeader('Content-Type');
					try {
						cb(JSON.parse(request.responseText));
					}catch(err) {
						cb(err);
					}
		}
	}
}
getJsonObject((obj) => {
	var product = obj.products.product;
	var arr = [];
	//Add arr items photo , price , sale_price
	for(var i = 0; i<product.length; i++){
		var link = product[i].thumb_image_link.split(",",1);
		var price = product[i].price;
		var sale_price = product[i].sale_price;
		var title = product[i].title;
		var brand = product[i].brand;
		var item_link_value = {
			link : link,
			price : price,
			sale_price : sale_price,
			brand : brand,
			title : title	
		};
		arr.push(item_link_value);
	}
	firstBanner(arr);
	secondBanner(arr);
	thirdBanner(arr);
});
// First banner 1100x130
function firstBanner(arr){
	var element = document.querySelectorAll('.first-banner a >img');
	var price = document.querySelectorAll('.first-banner .text-area > p');
	let discount = document.querySelectorAll('.discount > p');
	let brand = document.querySelectorAll('.first-banner .brand-img a > p');
	console.log(brand);
	let j = 0;
	for ( let i = 0; i<12; i++){
		//let index = Math.floor(Math.random() * 174);
		element[i].src = arr[i].link;
		price[j].innerHTML = arr[i].price + " TL";
		price[j+1].innerHTML = arr[i].sale_price + " TL";
		discount[i].innerHTML =discountCalc(parseInt(arr[i].price),parseInt(arr[i].sale_price)) + " %"; 
		brand[i].innerHTML = arr[i].brand;
		j = j + 2
	}

}
//Second banner 300x250
function secondBanner(arr){
	let element = document.querySelectorAll('.second-banner a >img');
	let price = document.querySelectorAll('.second-banner .text-area > p');
	let discount = document.querySelectorAll('.discount-sm > p');
	let brand = document.querySelectorAll('.second-banner .brand-img a > p');
	let count = 0;
	for(let i = 0; i<2 ; i++){
		//let  i = Math.floor(Math.random() * 174);
		element[i].src = arr[i].link;
		price[count].innerHTML = arr[i].price + " TL";
		price[count+1].innerHTML = arr[i].sale_price + " TL";
		discount[i].innerHTML = discountCalc(parseInt(arr[i].price),parseInt(arr[i].sale_price)) + " %";
		brand[i].innerHTML = arr[i].brand;
		count = count + 2;
	}
}
//Third Banner 300x600
function thirdBanner(arr){
	//var i = Math.floor(Math.random() * 174);
	let element = document.querySelectorAll('.third-banner a >img');
	let price = document.querySelectorAll('.third-banner .text-area > p');
	let discount = document.querySelectorAll('.discount-xl > p ');
	let item = document.querySelectorAll('.item > p');
	element[0].src = arr[0].link;
	price[0].innerHTML = arr[0].price + " TL";
	price[1].innerHTML = arr[0].sale_price + " TL";
	discount[0].innerHTML = discountCalc(parseInt(arr[0].price),parseInt(arr[0].sale_price)) + " %";
	item[0].innerHTML = arr[0].brand;
	item[1].innerHTML = arr[0].title;
}

function discountCalc(price,sale_price){
	var discount = 100-(sale_price / price)*100;
	return parseInt(discount);
}
